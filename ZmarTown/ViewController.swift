//
//  ViewController.swift
//  ZmarTown
//
//  Created by Giuliano Tognarelli on 3/27/15.
//  Copyright (c) 2015 Giuliano Tognarelli. All rights reserved.
//

import UIKit
import MapKit



class ViewController: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var theMapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        var latitude: CLLocationDegrees = 48.399193
        var longitude: CLLocationDegrees = 9.99334
        var latDelta: CLLocationDegrees = 0.01 //Zoom when the app launches
        var longDelta: CLLocationDegrees = 0.01
        
        var theSpan: MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
        
        var initialLocation: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
        
        var theRegion:MKCoordinateRegion = MKCoordinateRegionMake(initialLocation, theSpan)
        
        self.theMapView.setRegion(theRegion, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

